import { TestBed } from '@angular/core/testing';

import { SpeedtestService } from './speedtest.service';

describe('SpeedtestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpeedtestService = TestBed.get(SpeedtestService);
    expect(service).toBeTruthy();
  });
});
