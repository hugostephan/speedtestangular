import { Component, OnInit } from '@angular/core';
import { SpeedtestService } from '../speedtest.service';

@Component({
  selector: 'app-speedtest',
  templateUrl: './speedtest.component.html',
  styleUrls: ['./speedtest.component.css']
})

/*
 * Speedtest component class : provides links between UI and processing
 */
export class SpeedtestComponent implements OnInit {
  data: any;
  downloadColor: string;
  meterBackground: string;
  progressColor: string;
  progress: any;
  speed: any;
  uiData: any;

  constructor(private speedtest: SpeedtestService) {}

  // init component : variables and UI
  ngOnInit() {
    this.meterBackground = /Trident.*rv:(\d+\.\d+)/i.test(navigator.userAgent) ? '#EAEAEA' : '#80808040';
    this.downloadColor = '#6060AA';
    this.uiData = null;
    this.progressColor = this.meterBackground;
    this.speed = 0;
    this.progress = '0 %' ;

    setTimeout(() => {
      this.initUI();
    }, 100);

    window.requestAnimationFrame =
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      ((callback): number => {
        setTimeout(callback, 1000 / 60);
        return 0;
      });

    this.frame();
  }

  // method to draw the progress bar
  drawMeter(context, amount: number, bk: string, fg: string, progress: number, prog: string) {
    const ctx = context.getContext('2d');
    const dp = window.devicePixelRatio || 1;
    const cw = context.clientWidth * dp;
    const ch = context.clientHeight * dp;
    const sizeScale = ch * 0.0055;

    if (context.width === cw && context.height === ch) {
      ctx.clearRect(0, 0, cw, ch);
    } else {
      context.width = cw;
      context.height = ch;
    }
    ctx.beginPath();
    ctx.strokeStyle = bk;
    ctx.lineWidth = 12 * sizeScale;
    ctx.arc(
      context.width / 2,
      context.height - 58 * sizeScale,
      context.height / 1.8 - ctx.lineWidth,
      -Math.PI * 1.1,
      Math.PI * 0.1
    );
    ctx.stroke();
    ctx.beginPath();
    ctx.strokeStyle = fg;
    ctx.lineWidth = 12 * sizeScale;
    ctx.arc(
      context.width / 2,
      context.height - 58 * sizeScale,
      context.height / 1.8 - ctx.lineWidth,
      -Math.PI * 1.1,
      amount * Math.PI * 1.2 - Math.PI * 1.1
    );
    ctx.stroke();
    if (typeof progress !== 'undefined') {
      ctx.fillStyle = prog;
      ctx.fillRect(
        context.width * 0.3,
        context.height - 16 * sizeScale,
        context.width * 0.4 * progress,
        4 * sizeScale
      );
    }
  }

  // update the UI every frame
  frame() {
    if (this !== undefined) {
      requestAnimationFrame(this.frame);
      this.updateUI(false);
    }
  }

  // init speed test UI
  initUI() {
    this.drawMeter(this.UI('dlMeter'), 0, this.meterBackground, this.downloadColor, 0, '');
    this.speed = '...';
  }

  // convert speed to mbps
  mbpsToAmount(s: number) {
    return 1 - 1 / Math.pow(1.3, Math.sqrt(s));
  }

  // Beautiful progress bar must oscillate apparently
  oscillate() {
    return 1 + 0.02 * Math.sin(Date.now() / 100);
  }

  // fetch element from DOM ()
  UI(id: string) {
    return document.getElementById(id);
  }

  // update speed test UI
  updateUI(forced: boolean) {
    // Error cases
    if (!forced && this.speedtest.getState() !== 1) {
      console.log('updateUI exited due to bad param \'forced\'');
      return;
    }
    if (this.uiData == null) {
      console.log('updateUI exited due to null uiData');
      return;
    }

    // parsing JSON uiData to get speed and progress
    const status: number = JSON.parse(this.uiData).testState;
    const downloadSpeed: any = JSON.parse(this.uiData).downloadSpeed;
    const downloadProgress: any = JSON.parse(this.uiData).downloadProgress;
    this.speed = (status === 1 && downloadSpeed === 0) ? '...' : downloadSpeed;
    this.progress = Math.round(downloadProgress * 100) + ' %';
    console.log('speed = ' + this.speed);

    // drawing progress bar
    this.drawMeter(
      this.UI('dlMeter'),
      this.mbpsToAmount(
        Number(downloadSpeed * (status === 1 ? this.oscillate() : 1))
      ),
      this.meterBackground,
      this.downloadColor,
      Number(downloadProgress),
      this.progressColor
    );
  }

  // toogle speedtest's start/stop
  startStop() {
    if (this.speedtest.getState() === 1) {
      // speedtest is running, abort
      this.speedtest.abort();
      this.data = null;
      this.initUI();
    } else {
      // speedtest is not running, begin
      this.speedtest.onupdate = data => {
        console.log('onupdate');
        this.uiData = data;
        this.updateUI(false);
      };
      this.speedtest.onend = aborted => {
        console.log('onend');
        this.updateUI(aborted);
      };
      this.speedtest.start();
    }
  }
}
