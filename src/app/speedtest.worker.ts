/*
	Speedtest HTML5/JS
	based on https://github.com/adolfintel/speedtest/
	GNU LGPLv3 License
*/

/*
 * Speed test web worker : do the speed test itself
 */
/// <reference lib="webworker" />
// -1=pending, 0=starting, 1=download test, 2=finished, 3=abort
let testState = -1;
// download speed in megabit/s with 2 decimal digits
let downloadSpeed: any;
// progress of download test 0-1
let downloadProgress = 0;
// test ID (sent back by telemetry if used, null otherwise)
let testId = null;

// test settings
let settings = {
  // set to true when in MPOT mode
  mpot: true,
  // max duration of download test in seconds
  downloadMaxTime: 15,
  // if set to true, tests will take less time on faster connections
  timeAuto: true,
  // time to wait in seconds before actually measuring dl speed (wait for TCP window to increase)
  downloadGraceTime: 1.5,
  // path to a large file or garbage.php, used for download test. must be relative to this js file
  downloadUrl: 'test.data',
  // number of download streams to use (can be different if enableQuirks is active)
  xhrDownloadMultistream: 6,
  // how much concurrent requests should be delayed
  xhrDownloadMultistreamDelay: 300,
  // 0=fail on errors, 1=attempt to restart a stream if it fails, 2=ignore all errors
  xhrDownloadIgoreErrors: 1,
  // if set to true, it reduces ram usage but uses the hard drive (useful with large download)
  xhrDownloadUseBlob: false,
  // size of chunks sent by garbage.php (can be different if enableQuirks is active)
  chunkSize: 100,
  // enable quirks for specific browsers. currently it overrides settings to optimize
  enableQuirks: true,
  // can be changed to compensatie for transport overhead. (see doc.md for some other values)
  overheadCompensationFactor: 1.06,
  // if set to true, speed will be reported in mebibits/s instead of megabits/s
  useMebibits: false
};

// array of currently active xhr requests
let xhr = null;
// timer used in tests
let interval = null;
// used to prevent multiple accidental calls to dlTest
let downloadCalled = false;
let downloadRun = false;

/*
  function used to determine whether we need a ? or an & as a separator
*/
function urlSep(url: string) {
  return url.match(/\?/) ? '&' : '?';
}

function finish() {
  // test is finished
  console.log('test is finished');
  testState = 2;
    // return status
  const t = testState;
  const ds = downloadSpeed;
  const dp = downloadProgress;
  const ti = testId;
  postMessage(
    JSON.stringify({
      testState: t,
      downloadSpeed: ds,
      downloadProgress: dp,
      testId: ti
    })
  );
}
/*
 * Event Handler function : called when the worker receive messages from main thread
 */
addEventListener('message', ({ data }) => {
  const t = testState;
  const ds = downloadSpeed;
  const dp = downloadProgress;
  const ti = testId;
  if (data.split === undefined) {
    return;
  } else {
    console.log('e = ' + JSON.stringify(data));
  }
  const params = data.split(' ');

  // sending dl state and progress
  if (params[0] === 'status') {
    if (testState < 2) {
      console.log('event onmessage');
      // return status
      postMessage(
        JSON.stringify({
          testState: t,
          downloadSpeed: ds,
          downloadProgress: dp,
          testId: ti
        })
      );
    }
  }

  // starting speed test
  if (params[0] === 'start' && testState === -1) {
    // start new test
    testState = 0;
    try {
      const ua = navigator.userAgent;

      // quirks for specific browsers (to move smwh else)
      if  (settings.enableQuirks ||
          (typeof settings.enableQuirks !== 'undefined' && settings.enableQuirks)) {
        if (/Edge.(\d+\.\d+)/i.test(ua)) {
          if (typeof settings.xhrDownloadMultistream === 'undefined') {
            // edge more precise with 3 download streams
            settings.xhrDownloadMultistream = 3;
          }
        }
        if (/Chrome.(\d+)/i.test(ua) && !!self.fetch) {
          if (typeof settings.xhrDownloadMultistream === 'undefined') {
            // chrome more precise with 5 streams
            settings.xhrDownloadMultistream = 5;
          }
        }
      }
    } catch (e) {
      console.log('Unexpected error initializing speed test worker : ' + e);
    }
    // run the tests
    console.log(JSON.stringify(settings));
    dlTest(finish);
  }

  // aborting speed test
  if (params[0] === 'abort') {
    // abort command
    if (testState >= 2) {
      console.log('already finished/aborted');
      return;
    }
    console.log('manually aborted');

    // stop all xhr activity
    clearRequests();

    // clearing everything and notice test has been aborted
    if (interval) {
      console.log('clearing interval interval');
      clearInterval(interval);
    }
    testState = 3;
    downloadSpeed = '';
    downloadProgress = 0;
  }
});

// stops all XHR activity, aggressively
function clearRequests() {
  console.log('stopping pending XHRs');
  if (xhr) {
    for (let i = 0; i < xhr.length; i++) {
      try {
        xhr[i].onprogress = null;
        xhr[i].onload = null;
        xhr[i].onerror = null;
      } catch (e) {}
      try {
        xhr[i].upload.onprogress = null;
        xhr[i].upload.onload = null;
        xhr[i].upload.onerror = null;
      } catch (e) {}
      try {
        xhr[i].abort();
      } catch (e) {}
      try {
        delete xhr[i];
      } catch (e) {}
    }
    xhr = null;
  }
}

// download test, calls done function when it's over
function dlTest(done) {
  // dlTest already called?
  if (downloadCalled) {
    return;
  } else {
    downloadCalled = true;
  }

  // starting test
  testState = 1;
  // total number of loaded bytes
  let totLoaded = 0.0;
  // timestamp when test was started
  let startT = new Date().getTime();
  // how many milliseconds the test has been shortened by (higher on faster connections)
  let bonusT = 0;
  // set to true after the grace time is past
  let graceTimeDone = false;
  // set to true if a stream fails
  let failed = false;
  // requests array
  xhr = [];

  // function to create a download stream. streams are slightly delayed not to end at the same time
  const testStream = (i, delay) => {
    setTimeout(() => {
      // delayed stream ended up starting after the end of the download test
      if (testState !== 1) {
        return;
      }
      console.log('dl test stream started ' + i + ' ' + delay);

      // number of bytes loaded last time onprogress was called
      let prevLoaded = 0;
      const x = new XMLHttpRequest();
      xhr[i] = x;

      xhr[i].onprogress = event => {
        console.log('dl stream progress event ' + i + ' ' + event.loaded);
        // just in case  XHR is still running after the download test
        if (testState !== 1) {
          try {
            x.abort();
          } catch (e) {}
        }

        // progress event, add number of new loaded bytes to totLoaded
        const loadDiff = event.loaded <= 0 ? 0 : event.loaded - prevLoaded;
        if (isNaN(loadDiff) || !isFinite(loadDiff) || loadDiff < 0) {
          return;
        } else {
          totLoaded += loadDiff;
          prevLoaded = event.loaded;
        }
      };

      xhr[i].onload = () => {
        // the large file has been loaded entirely, start again
        console.log('dl stream finished ' + i);

        // reset the stream data to empty ram
        try {
          xhr[i].abort();
        } catch (e) {}
        testStream(i, 0);
      };

      xhr[i].onerror = () => {
        // error
        console.log('dl stream failed ' + i);
        if (settings.xhrDownloadIgoreErrors === 0) {
          failed = true;
        }

        // abort
        try {
          xhr[i].abort();
        } catch (e) {}
        delete xhr[i];

        // restart stream
        if (settings.xhrDownloadIgoreErrors === 1) {
          testStream(i, 0);
        }
      };

      // xhr type
      try {
        if (settings.xhrDownloadUseBlob) {
          xhr[i].responseType = 'blob';
        } else {
          xhr[i].responseType = 'arraybuffer';
        }
      } catch (e) {}

      // randomize query string by adding var to prevent caching
      xhr[i].open(
        'GET',
        settings.downloadUrl +
          urlSep(settings.downloadUrl) +
          (settings.mpot ? 'cors=true&' : '') +
          'r=' +
          Math.random() +
          '&ckSize=' +
          settings.chunkSize,
        true
      );

      // send xhr
      xhr[i].send();
    }, 1 + delay);
  };

  // open streams
  for (let i = 0; i < settings.xhrDownloadMultistream; i++) {
    testStream(i, settings.xhrDownloadMultistreamDelay * i);
  }

  // every 200ms, update downloadSpeed
  interval = setInterval(() => {
    console.log('DL: ' + downloadSpeed + (graceTimeDone ? '' : ' (in grace time)'));
    const t = new Date().getTime() - startT;
    if (graceTimeDone) {
      downloadProgress = (t + bonusT) / (settings.downloadMaxTime * 1000);
    }
    if (t < 200) {
      return;
    }
    if (!graceTimeDone) {
      if (t > 1000 * settings.downloadGraceTime) {
        if (totLoaded > 0) {
          // if the connection is so slow that we didn't get a single chunk yet, do not reset
          startT = new Date().getTime();
          bonusT = 0;
          totLoaded = 0.0;
        }
        graceTimeDone = true;
      }
    } else {
      const speed = totLoaded / (t / 1000.0);
      // decide how much to shorten the test, the test is shortened by the bonusT calculated here
      if (settings.timeAuto) {
        const bonus = (6.4 * speed) / 100000;
        bonusT += bonus > 800 ? 800 : bonus;
      }

      // update status, speed is multiplied by 8 to go from bytes to bits, overhead compensation is
      // applied, then everything is divided by 1048576 or 1000000 to go to megabits/mebibits
      downloadSpeed = (
        (speed * 8 * settings.overheadCompensationFactor) /
        (settings.useMebibits ? 1048576 : 1000000)
      ).toFixed(2);

      // test is over, stop streams and timer
      if ((t + bonusT) / 1000.0 > settings.downloadMaxTime || failed) {
        if (failed || isNaN(downloadSpeed)) {
          downloadSpeed = 'Fail';
        }
        console.log('clearing interval interval');
        clearRequests();
        clearInterval(interval);
        downloadProgress = 1;
        console.log(
          'dlTest: ' +
            downloadSpeed +
            ', took ' +
            (new Date().getTime() - startT) +
            'ms'
        );
        done();
      }
    }
  }, 200);
}
