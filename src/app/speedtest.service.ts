import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

/**
 * Speedtest service class : encapsulate a web worker to process download
 */
export class SpeedtestService {
  onend: any;
  onupdate: any;
  previousData: any;
  settings: any;
  state: number;
  updater: any;
  worker: any;

  constructor() {
    this.previousData = null;
    // -1=pending, 0=starting, 1=download test, 2=finished, 3=abort
    this.state = 0;
    // settings for the speedtest worker
    this.settings = {};
  }

  /**
   * Returns the state of the test:
   * 0=adding settings, 1=adding servers, 2=server selection done, 3=test running, 4=done
   */
  getState() {
    return this.state;
  }

  /**
   * Starts the test.
   * During the test, the onupdate(data) callback function will be called periodically with data
   * from the worker. At the end of the test, the onend(aborted) function will be called with a
   * boolean telling you if the test was aborted or if it ended normally.
   */
  start() {
    if (this.state === 1) {
      throw new Error('Test already running');
    }
    // Try to instantiate a new speedtest web worker (see https://angular.io/guide/web-worker)
    if (typeof Worker !== 'undefined') {
      this.worker = new Worker('./speedtest.worker', { type: 'module' });

      // When the worker send messages
      this.worker.onmessage =  ({ data }) => {
        console.log('onmessage');

        // Checking if the received data is actually new
        if (data === this.previousData) {
          return;
        } else {
          this.previousData = data;
        }

        // Try to update UI with new data
        try {
          if (this.onupdate) {
            console.log('updating from service');
            this.onupdate(data);
          } else {
            console.log('error : onupdate function cannot be found');
          }
        } catch (e) {
          console.error('Speedtest onupdate event threw exception: ' + e);
        }

        // Test's end
        if (JSON.parse(data).testState >= 2) {
          try {
            if (this.onend) {
              this.onend(JSON.parse(data).testState === 3);
            }
          } catch (e) {
            console.error('Speedtest onend event threw exception: ' + e);
          }
          console.log('clearing interval updater');
          clearInterval(this.updater);
          this.state = 2;
        }
      };

      // Periodically fetching status from worker
      this.updater = setInterval(
        () => {
          this.worker.postMessage('status');
        },
        200
      );

      // starting the worker
      this.state = 1;
      this.worker.postMessage('start ' + JSON.stringify(this.settings));
    } else {
      // Web Workers are not supported in this environment.
      // You should add a fallback so that your program still executes correctly.
      // TO DO ?
    }
  }

  /**
   * Aborts the test while it's running.
   */
  abort() {
    if (this.state < 1) {
      throw new Error('You cannot abort a test that\'s not started yet');
    } else if (this.state < 2) {
      this.worker.postMessage('abort');
    } else {
      throw new Error('Unexpected test state when aborting');
    }
  }
}
